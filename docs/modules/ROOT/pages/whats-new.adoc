= What's New in Antora {page-component-version}
:doctype: book
:url-releases-asciidoctor: https://github.com/asciidoctor/asciidoctor/releases
:url-releases-asciidoctorjs: https://github.com/asciidoctor/asciidoctor.js/releases
:url-gitlab: https://gitlab.com
:url-git-antora: {url-gitlab}/antora/antora
:url-issues: {url-git-antora}/issues
:url-milestone-3-0-0: {url-issues}?scope=all&state=closed&label_name%5B%5D=%5BVersion%5D%203.0.0
:url-mr: {url-git-antora}/merge_requests

IMPORTANT: The added features and changes described below are in *ALPHA* and subject to change.

= Antora 3.0.0-alpha.3

_**Release date:** 2021.04.15 | *Issue label:* {url-milestone-3-0-0}[3.0.0^]_

== Deprecations and breaking changes notice

The following deprecations and breaking changes will be final with the release of Antora 3.0.

=== Dependencies

Antora now automatically depends on the latest patch version of Asciidoctor.js 2.2 (e.g., 2.2.3).
Support for Asciidoctor.js 1.5.9 has been dropped.

=== Specifying the versionless component version

Since the first release of Antora, the version `master` has been given special meaning to identify a versionless component version.
Using that term for this purpose was a mistake and we're correcting it.

When a component version is "`versionless`", it means the URL for that component version and its resources do not have a version segment (e.g., [.path]_/component-name/module-name/page-name.html_ instead of [.path]_/component-name/module-name/version-name/page-name.html_).
In Antora 3.0, we're deprecating the use of the version `master` for this purpose.
The reason we're phasing out this term is because it's not descriptive, it infers that the version is coupled to the branch (which it's not), and it glorifies an immoral system based on human exploitation.
In short, the term just isn't appropriate and we want to move away from it.

Instead, you should identify a versionless component version by assigning the tilde symbol (`~`) (shorthand for `null`) to the version key in the component descriptor (i.e., [.path]_antora.yml_).

.antora.yml for a versionless component version
[source,yaml]
----
name: component-name
version: ~
----

As expected, when the version key is assigned `~`, Antora doesn't include the version segment in the component version's page URLs (e.g., [.path]_/component-name/module-name/page-name.html_).
Although rare, if you ever need to refer to a resource in a versionless version, you can do so using the `_` keyword in the resource ID (e.g., `_@page.html`).

== Resolved issues

IMPORTANT: The added features and changes described below are in *ALPHA* and subject to change.

=== Added

Issue {url-issues}/669[#669^]:: Allow value of the `version` key in a component descriptor file to be `~` (shorthand for `null`) to indicate a versionless component version.
Null is assigned using the tilde symbol (`~`) or the keyword `null`.
Empty string is also accepted, but not as elegant.
Internally, the value is coerced to empty string for practical purposes.
Issue {url-issues}/669[#669^]:: If the version is empty (`version: ~`), don't add a version segment to `pub.url` and `out.path` (even if it's a prerelease).
Issue {url-issues}/669[#669^]:: Sort the versionless version above all other versions (semantic and non-semantic) that belong to the same component.
Assign the fallback _default_ as the display version if the version is empty and the `display_version` key isn't specified.
If `prerelease` is set in the component descriptor to a string value, use that as the fallback display version instead.
Issue {url-issues}/669[#669^]:: If the version is not specified on an alias that specifies an unknown component, set the version to empty string.
We expect this change to be internal and not affect any sites.
Issue {url-issues}/669[#669^]:: Add support for `_` keyword to refer to an empty version in a resource ID (e.g., `_@page.html`).

=== Changed

Issue {url-issues}/522[#522^]:: Upgrade to Asciidoctor.js 2.2.3 and allow installation of newer patch versions automatically.
Issue {url-issues}/731[#731^]:: Add support for Node.js 12 and Node.js 14.
Run tests nightly on Node.js 12 and 14 (in addition to Node.js 10).

=== Fixed

Issue {url-issues}/663[#663^]:: Don't crash if a stem block is empty.

=== Deprecated

Issue {url-issues}/669[#669^]:: Deprecate the value `master` to represent an empty (versionless) version when assigned to the `version` key in a component descriptor file; replace with the tilde symbol (`~`).

=== Removed

Issue {url-issues}/522[#522^]:: Drop support for Asciidoctor.js 1.5.9.
By using Antora 3, you will automatically be upgraded to using Asciidoctor.js 2.2.x.

= Antora 3.0.0-alpha.2

_**Release date:** 2021.04.08 | *Issue label:* {url-milestone-3-0-0}[3.0.0^]_

== Resolved issues

IMPORTANT: The added features and changes described below are in *ALPHA* and subject to change.

=== Added

Issue {url-issues}/150[#150^]:: Allow extracted UI bundle to be loaded from directory.
Issue {url-issues}/694[#694^]:: Store refname of content source on `src.origin.refname` property of virtual file.

=== Fixed

Issue {url-issues}/698[#698^]:: Add `redirect` modifier to splat alias rewrite rule for nginx (when redirect-facility=nginx).
Issue {url-issues}/700[#700^]:: Show error message with backtrace (if available) when `--stacktrace` option is set, even if the stack property is missing.

[#removed-alpha-2]
=== Removed

Issue {url-issues}/689[#689^]:: Remove deprecated `page-relative` attribute; superseded by `page-relative-src-path`.

= Antora 3.0.0-alpha.1

_**Release date:** 2020.09.29 | *Issue label:* {url-milestone-3-0-0}[3.0.0^]_

== Deprecations and breaking changes notice

The following deprecations and breaking changes will be final with the release of Antora 3.0.

=== Syntax

The ability to use parent references in the target of the AsciiDoc image macro (e.g., `image::../../../module-b/_images/image-filename.png[]`) has been removed.
Replace any such image targets with resource IDs before upgrading.

Antora has added the _.adoc_ file extension to a xref:page:page-id.adoc#id-coordinates[page coordinate] in page aliases and xrefs whenever it wasn't specified by the writer.
This fallback mechanism has been deprecated in Antora 3.0 to make way for using non-AsciiDoc pages in the xref facility.
Review the page IDs in your xrefs and `page-aliases` attributes to ensure the _.adoc_ extension is specified before upgrading.

=== Dependencies

Support for Node.js 8 has been dropped; the minimum required version is now Node 10.

See the <<removed-alpha-1>> and <<deprecated-alpha-1>> sections for the entire list of breaking changes.

== Resolved issues

IMPORTANT: The added features and changes described below are in *ALPHA* and subject to change.

=== Added

Issue {url-issues}/314[#314^]:: Add `urls.latest_version_segment_strategy` key to playbook schema.
Issue {url-issues}/314[#314^]:: Add `urls.latest_version_segment` and `urls.latest_prerelease_version_segment` keys to playbook schema.
Issue {url-issues}/314[#314^]:: Replace latest version and/or prerelease version segment in out path and pub URL (unless version is master) with symbolic name, if specified.
Issue {url-issues}/314[#314^]:: Define `latestPrerelease` property on component version (if applicable) and use when computing latest version segment.
Issue {url-issues}/314[#314^]:: Use redirect facility to implement redirect:to and redirect:from strategies for version segment in out path / pub URL of latest and latest prerelease versions.
Issue {url-issues}/355[#355^]:: Assign author to `page` object in UI model
Issue {url-issues}/425[#425^]:: Assign primary alias to `rel` property on target page.
Issue {url-issues}/605[#605^]:: Extract method to register start page for component version (`ContentCatalog#registerComponentVersionStartPage`).
Issue {url-issues}/615[#615^]:: Store computed web URL of content source on `src.origin.webUrl` property of virtual file.

=== Changed

Issue {url-issues}/314[#314^]:: Register all component versions before adding files to content catalog.
Issue {url-issues}/425[#425^]:: Follow aliases when computing version lineage for page and canonical URL in UI model.
Issue {url-issues}/598[#598^]:: Upgrade dependencies.
Issue {url-issues}/605[#605^]:: Only register start page for component version in `ContentCatalog#registerComponentVersion` if value of `startPage` property in descriptor is truthy.
Issue {url-issues}/605[#605^]:: Call `ContentCatalog#registerComponentVersionStartPage` in content classifier to register start page after adding files (instead of before).
Issue {url-issues}/681[#681^]:: Don't use global git credentials path if custom git credentials path is specified, but does not exist.
Issue {url-issues}/682[#682^]:: Replace the fs-extra dependency with calls to the promise-based fs API provided by Node.
Issue {url-issues}/689[#689^]:: Require page ID spec for start page to include the [.path]_.adoc_ file extension.
Issue {url-issues}/689[#689^]:: Require page ID spec target in xref to include the [.path]_.adoc_ file extension.
Issue {url-issues}/689[#689^]:: Make check for [.path]_.adoc_ extension in value of xref attribute on image more accurate.
Issue {url-issues}/689[#689^]:: Interpret every non-URI image target as a resource ID.
Issue {url-issues}/689[#689^]:: Rename exported `resolveConfig` function in AsciiDoc loader to `resolveAsciiDocConfig`; retain `resolveConfig` as deprecated alias.
Issue {url-issues}/693[#693^]:: Defer assignment of `mediaType` and `src.mediaType` properties on virtual file to content classifier.
Issue {url-issues}/693[#693^]:: Enhance `ContentCatalog#addFile` to update `src` object if missing required properties, including `mediaType`.

=== Fixed

Issue {url-issues}/678[#678^]:: Add support for optional option on include directive to silence warning if target is missing.
Issue {url-issues}/680[#680^]:: Show sensible error message if cache directory cannot be created.
Issue {url-issues}/695[#695^]:: Don't crash when loading or converting AsciiDoc document if content catalog is not passed to `loadAsciiDoc`.

[#deprecated-alpha-1]
=== Deprecated

Issue {url-issues}/689[#689^]:: Deprecate `getAll` method on ContentCatalog; superseded by `getFiles`.
Issue {url-issues}/689[#689^]:: Deprecate `getAll` method on UiCatalog; superseded by `getFiles`.
Issue {url-issues}/689[#689^]:: Deprecate exported `resolveConfig` function in AsciiDoc loader.
Issue {url-issues}/689[#689^]:: Deprecate use of page ID spec without .adoc file for page alias.
Issue {url-issues}/689[#689^]:: Deprecate use of non-resource ID spec (e.g., parent path) as target of include directive.
Issue {url-issues}/689[#689^]:: Deprecate `getAll` method on site catalog; superseded by `getFiles`.
Issue {url-issues}/689[#689^]:: Deprecate the `--google-analytics-key` CLI option; superseded by the `--key` option.

[#removed-alpha-1]
=== Removed

Issue {url-issues}/679[#679^]:: Drop support for Node.js 8 and set minimum required version to 10.
Issue {url-issues}/689[#689^]:: Remove `pull` key from `runtime` category in playbook; superseded by `fetch` key.
Issue {url-issues}/689[#689^]:: Remove `ensureGitSuffix` key from `git` category in playbook file (but not playbook model); renamed to `ensure_git_suffix`.
Issue {url-issues}/689[#689^]:: Remove fallback to resolve site-wide AsciiDoc config in `classifyContent` function.
Issue {url-issues}/689[#689^]:: Drop `latestVersion` property on component version object; superseded by `latest` property.
Issue {url-issues}/689[#689^]:: Remove deprecated `getComponentMap` and `getComponentMapSortedBy` methods on `ContentCatalog`.

////
[#thanks-3-0-0]
== Thanks

Most important of all, a huge *thank you!* to all the folks who helped make Antora even better.

We want to call out the following people for making contributions to this release:
////

// Contributors
////
({url-issues}/553[#553^])
({url-mr}/405[!405^])

Antonio ({url-gitlab}/bandantonio[@bandantonio^])::
Karl Dangerfield ({url-gitlab}/obayozo[@obayozo^])::
Rob Donnelly ({url-gitlab}/rfdonnelly[@rfdonnelly^])::
Ewan Edwards ({url-gitlab}/eedwards[@eedwards^])::
James Elliott ({url-gitlab}/DeepSymmetry[@DeepSymmetry^])::
gotwf ({url-gitlab}/gotwf[@gotwf^])::
Guillaume Grossetie ({url-gitlab}/g.grossetie[@g.grossetie^])::
Chris Jaquet ({url-gitlab}/chrisjaquet[@chrisjaquet])::
David Jencks ({url-gitlab}/djencks[@djencks^])::
Jared Morgan ({url-gitlab}/jaredmorgs[@jaredmorgs^])::
Daniel Mulholland ({url-gitlab}/danyill[@danyill^])::
Alexander Schwartz ({url-gitlab}/ahus1[@ahus1^])::
Ben Walding ({url-gitlab}/bwalding[@bwalding^])::
Coley Woyak ({url-gitlab}/coley.woyak.saagie[@coley.woyak.saagie^])::
Anthony Vanelverdinghe ({url-gitlab}/anthonyv.be[@anthonyv.be^])::
////
